#!/usr/bin/env bash

if [ -z $3 ]
    then
    echo Usage: >& 2
    echo "    \$ $0 directory host1 host2" >& 2
    exit 1
fi

DIR=$1/$RANDOM
HOST1=$2
HOST2=$3

function prepare {
ssh -T $HOST1 << EOF
if [ -e $DIR ]
    then echo Abort: Directory $DIR already exists >& 2; exit 1
    else mkdir -p $DIR
fi
EOF
if [ ! $? ]; then echo "Abort!"; exit 1; fi
}

function cleanup { ssh -T $HOST1 rm -rf $DIR; }

function stalelookup {
echo -n "stale negative lookup on open... "
FILE=$DIR/test1

# create negative cache entry on HOST2
ssh -T $HOST2 "cat $FILE 2>/dev/null"
# create the file through HOST1
ssh -T $HOST1 "echo foo > $FILE"
# now open file on HOST2
ssh -T $HOST2 << EOF
cd $DIR
python << EOF2
import errno
try:
    open('$FILE', 'r')
    print 'OK'
except IOError, e:
    if e.errno == errno.ENOENT:
        print 'FAIL'
    else:
        raise
EOF2
ret=$?
rm -rf $DIR
exit $ret
EOF
}

prepare
stalelookup
cleanup
